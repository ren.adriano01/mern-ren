const express = require("express");
const app = express();
const mongoose = require("mongoose");
const InfoModel = require('./models/info');
const cors = require("cors");

app.use(express.json());
app.use(cors());

mongoose.connect("mongodb+srv://renadriano:8Q35KpnQ3tQOW7ws@cict-ren.i1c8hkv.mongodb.net/cars?retryWrites=true&w=majority");

app.get("/getInfo", (req, res) => {
    InfoModel.find({}, (err, result) => {
        if (err) {
            res.json(err);
        }   
        else{
            res.json(result);
        } 
    }); 
});

app.post("/createInfo", async (req, res) => {
    const info = req.body;
    const newInfo = new InfoModel(info);
    await newInfo.save();
    res.json(info);
});

app.put('/updateInfo', async (req, res) => {
    const newModel = req.body.newModel;
    const id = req.body.id;
    console.log(newModel, id);
    
    try{
        await InfoModel.findById(id, (error, modelUpdate) => {
            modelUpdate.model = newModel;
            modelUpdate.save();
        });
    }
    catch(err){
        console.log(err);
    }
    res.send("Updated!");
});

app.delete("/deleteInfo/:id", async (req, res) => {
    const id = req.params.id
    await InfoModel.findByIdAndRemove(id).exec();
    res.send("Item Deleted!");
});


app.listen(3001, () => {
    console.log("SERVER RUNS PERFECTLY!");
});