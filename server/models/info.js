const mongoose = require("mongoose");

const InfoSchema = new mongoose.Schema({
    manufacturer:{
        type: String,
        required: true,
    },

    model:{
        type: String,
        required: true,
    },

    country:{
        type: String,
        required: true,
    },

    year:{
        type: Number,
        required: true,
    }
});

const InfoModel = mongoose.model("info", InfoSchema);
module.exports = InfoModel;