import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { useState, useEffect } from "react";
import Axios from "axios";


function App() {
  const [listOfInfo, setListOfInfo] = useState([]); 
  const [manufacturer, setManufacturer] = useState("");
  const [model, setModel] = useState("");
  const [country, setCountry] = useState("");
  const [year, setYear] = useState(0);


  useEffect(() => {
    Axios.get("http://localhost:3001/getInfo").then((response) => {
      setListOfInfo(response.data);
    })
  }, [])

  const saveRecord = () => {
    window.location.reload();
    Axios.post("http://localhost:3001/createInfo", {
      manufacturer: manufacturer,
      model: model,
      country: country,
      year: year,
    }).then((response) => {
      setListOfInfo([...listOfInfo, {
        manufacturer: manufacturer,
        model: model,
        country: country,
        year: year,
      }]);
    });
  };

  const updateInfo = (id) => {
    const newModel = prompt("Enter new model: ");
    Axios.put("http://localhost:3001/updateInfo", {newModel: newModel, id: id}).then(() => {
      setListOfInfo(listOfInfo.map((info) => {
        return info._id === id ? {_id: id, manufacturer: info.manufacturer, model: newModel, country: info.country, year: info.year} : info;
      }))
    })
  };

  const deleteInfo = (id) => {
    const choice = window.confirm("Do you really want to delete this record? ");
    if (choice === true){
      Axios.delete(`http://localhost:3001/deleteInfo/${id}`).then(() => {
        setListOfInfo(listOfInfo.filter((info) => {
          return info._id !== id;
          })
        );  
      });
    } 
  };

  const clear = () => {
      document.getElementById("txt1").value="";
      document.getElementById("txt2").value="";
      document.getElementById("txt3").value="";
      document.getElementById("txt4").value="";
  };

  return <Form className="p-3">
      
      <Form.Group className="w-50" controlId="CarInfo">
        <h3>Enter Information</h3>
        <Form.Label>Manufacturer</Form.Label>
        <Form.Control type="text" id="txt1" placeholder="Enter Manufacturer" onChange={(event) => { setManufacturer(event.target.value);}}/>
        <Form.Label>Model</Form.Label>
        <Form.Control type="text" id="txt2" placeholder="Enter Model" onChange={(event) => { setModel(event.target.value);}}/>
        <Form.Label>Country of Origin</Form.Label>
        <Form.Control type="text" id="txt3" placeholder="Enter Country" onChange={(event) => { setCountry(event.target.value);}}/>
        <Form.Label>Year</Form.Label>
        <Form.Control type="text" id="txt4" placeholder="Enter Year" onChange={(event) => { setYear(event.target.value);}}/>
      </Form.Group>
      <br/>
      <Button variant="primary" type="button" onClick={() => {saveRecord();}}>
        Save Record
      </Button>&nbsp;
      <Button variant="primary" type="button" onClick={() => {clear();}}>
        Clear
      </Button>
      <hr width="50%"></hr>
      <h3>Car Information</h3>
      {listOfInfo.map((info) => {
          return(
          <div> 
            <b>Manufacturer:</b> {info.manufacturer}&emsp; 
            <b>Model:</b> {info.model} &emsp; 
            <b>Country:</b> {info.country} &emsp; 
            <b>Year:</b> {info.year} &emsp; 
            <Button variant="primary" type="button" onClick={() => {updateInfo(info._id);}}>Update</Button>&nbsp; 
            <Button variant="primary" type="button" onClick={() => {deleteInfo(info._id);}}>Delete</Button><br/>
            <br/><br/>
          </div>
          )
      })}
    </Form>
}

export default App;